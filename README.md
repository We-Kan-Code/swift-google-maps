# API call handling on Google Maps for iOS using Swift #

* Swift 5
* MVVM architecture

### What is this repository for? ###

* This version shows a sample project for efficiently handling GET calls while moving the google maps to show nearby markers.

### Frameworks Used ###

Cocoapods will install these frameworks 

* Alamofire
* GoogleMaps

### How do I get set up? ###

* Install cocoapods
	* In Terminal, cd <project root folder>
	* pod install
* In AppConfig.swift
    * Google maps demo key is given. 
    * A demo key is used for the public stations API. 
    
And you will be good to go.


### Sample Project ###

* The app will fetch nearby stations list from a public API
* Nearby stations will be marked on Map
* As the user swipes through the map, the updated list of nearest stations will be fetched and displayed. 
* On continuous swipes in map, only the last swipe will fetch the nearby stations and the previous calls will be cancelled.



