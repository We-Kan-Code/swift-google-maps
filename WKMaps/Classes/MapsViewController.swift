//
//  MapsViewController.swift
//  WKMaps
//
//  Created by Brian on 21/11/19.
//  Copyright © 2019 WeKan. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps

class MapsViewController: UIViewController {
    
    @IBOutlet weak var mapView: GMSMapView!
    
    var viewModel: MapsViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        viewModel = MapsViewModel()
        
        //Setup maps
        setupMaps()
        
        //Check for permissions and get current location
        getMyLocation()
        
        //Add marker for current location
//        addCurrentLocationMarker()
        
        //Move camera to current location
        moveCameraToCurrentLocation()
        
        //Get Stations List
        fetchStationsList()
        
    }
    
    //MARK: - Map functions
    
    /// Setup / load google maps on screen
    func setupMaps() {
        mapView.delegate = self
        mapView.isMyLocationEnabled = false
        mapView.settings.myLocationButton = false
        mapView.padding = UIEdgeInsets(top: 50, left: 0, bottom: 0, right: 0)
    }
    
    /// Check for permissions and get current location.
    /// Keep this updated while moving
    func getMyLocation() {
        print("getMyLocation")
        if CLLocationManager.locationServicesEnabled() {
            viewModel.locationManager = CLLocationManager.init()
            viewModel.locationManager?.delegate = self
            viewModel.locationManager?.distanceFilter = kCLDistanceFilterNone
            viewModel.locationManager?.desiredAccuracy = kCLLocationAccuracyBest
            viewModel.locationManager?.requestAlwaysAuthorization()
            viewModel.locationManager?.requestWhenInUseAuthorization()
            //                viewModel.locationManager?.requestLocation()
            //                viewModel.locationManager?.startMonitoringSignificantLocationChanges()
            viewModel.locationManager?.startUpdatingLocation()
        } else {
            print("Location services are not enabled")
        }
    }
    
    /// Adds a marker on the current location
    func addCurrentLocationMarker() {
        guard let mylocation = viewModel.currentLocation else {
            print("EMPTY viewModel.currentLocation")
            return
        }
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: mylocation.latitude, longitude: mylocation.longitude)
        marker.title = "My Location"
        marker.snippet = "You can find me here"
        marker.icon = UIImage.init(named: "myLocation")
        marker.map = mapView
    }
    
    /// Moves the camera focus to the current location
    func moveCameraToCurrentLocation() {
        guard let lat = viewModel.currentLocation?.latitude,
            let lng = viewModel.currentLocation?.longitude else {
                print("EMPTY mapView.myLocation")
                return
        }
        let camera = GMSCameraPosition.camera(withLatitude: lat ,longitude: lng , zoom: defaultZoomLevel)
        self.mapView.animate(to: camera)
    }
    
    /// Make the API call to get the nearest station list in the visible radius.
    func fetchStationsList() {
        viewModel.getNearestStations(onAPISuccess: { (successMsg) in
            self.dropMarkersOnMap()
        }, onAPIFailure: { (errorMsf) in
            
        })
    }
    
    /// Add markers for the stations list on map, by clearing the existing markers if any.
    func dropMarkersOnMap() {
        print("dropMarkersOnMap: \(String(describing: viewModel.stations?.fuelStations?.count))")
        
        guard let stationsCount = viewModel.stations?.fuelStations?.count else {
            return
        }
        
        //Remove all markers
        mapView.clear()
        
        // drop current location marker
        addCurrentLocationMarker()
        
        // drop station markers
        for i in 0..<stationsCount {
            guard let fuelStation = viewModel.stations?.fuelStations![i] else {
                return
            }
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude: fuelStation.latitude!, longitude: fuelStation.longitude!)
            marker.title = fuelStation.stationName
            marker.snippet = (fuelStation.streetAddress ?? "") + ", " + (fuelStation.city ?? "")
            marker.map = mapView
        }
    }
    
    /// Get the visible center coordinate of the  google map
    func getMapCenterCoordinate() {
        let centerPoint = mapView.center
        viewModel.mapCenterLocation = mapView.projection.coordinate(for: centerPoint)
    }
    
    /// Get the center coordinate at the top of screen, ie, find centerX coordinate considering yAxis = 0
    func getTopCenterCoordinate() -> CLLocationCoordinate2D {
        // to get coordinate from CGPoint of your map
        let topCenter = mapView.convert(CGPoint(x: mapView.frame.size.width / 2.0, y: 0), from: mapView)
        let point = self.mapView.projection.coordinate(for: topCenter)
        return point
    }
    
    /// Get the visible radius of the map
    func getRadius() {
        guard let centerLocationCoord = viewModel.mapCenterLocation else {
            return
        }
        let centerLocation = CLLocation(latitude: centerLocationCoord.latitude, longitude: centerLocationCoord.longitude)
        let topCenterCoordinate = self.getTopCenterCoordinate()
        let topCenterLocation = CLLocation(latitude: topCenterCoordinate.latitude, longitude: topCenterCoordinate.longitude)
        let radiusInMetres = CLLocationDistance.init(centerLocation.distance(from: topCenterLocation))
        var radiusInMiles = radiusInMetres * 0.000621371
        if radiusInMiles > 0.0 && radiusInMiles < 1.0 {
            radiusInMiles = 1.0
        }
        viewModel.visibleRadius = radiusInMiles < 500 ? radiusInMiles : defaultRadiusInMiles
    }
    

    //MARK: - Button Actions
    
    @IBAction func refreshAction(_ sender: Any) {
        getMyLocation()
        addCurrentLocationMarker()
        moveCameraToCurrentLocation()
    }
    
    /// Current location button action.
    /// - Gets updated current location and update the current location marker
    /// - Moves focus to the current location and load nearest  stations
    @IBAction func myLocationBtnAction(_ sender: Any) {
        getMyLocation()
        addCurrentLocationMarker()
        moveCameraToCurrentLocation()
    }
    
    
}

// MARK: -

extension MapsViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("didUpdateLocations: \(locations)")
//        viewModel.currentLocation = locations[0].coordinate
        viewModel.currentLocation = CLLocationCoordinate2DMake(38.5783802, -121.4926)
        addCurrentLocationMarker()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            print("notDetermined")
        case .denied:
            print("denied")
        case .authorizedWhenInUse:
            viewModel.locationManager?.stopUpdatingLocation()
        case .authorizedAlways:
            viewModel.locationManager?.startUpdatingLocation()
        case .restricted:
            print("restricted")
        @unknown default:
            print("default")
            fatalError()
        }
    }
}

//MARK: -
extension MapsViewController: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        print("willMove")
        viewModel.cancelAnyOngoingRequests()
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        print("didChange position: (\(position.target.latitude), \(position.target.longitude)), Zoom: \(position.zoom)")
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        print("ideleAt position: (\(position.target.latitude), \(position.target.longitude)), Zoom: \(position.zoom)")
        getMapCenterCoordinate()
        getRadius()
        fetchStationsList()
    }
    
}

