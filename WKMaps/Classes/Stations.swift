//
//  Stations.swift
//  WKMaps
//
//  Created by Brian on 19/11/19.
//  Copyright © 2019 WeKan. All rights reserved.
//

import Foundation


class Stations: Codable {
    var stationLocatorUrl: String?
    var totalResults: String?
    var fuelStations: [FuelStation]?

    private enum CodingKeys: String, CodingKey {
        case stationLocatorUrl = "station_locator_url"
        case totalResults = "total_results"
        case fuelStations = "fuel_stations"
    }

    init() {
    }
    
    // MARK: - Codable
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(stationLocatorUrl, forKey: .stationLocatorUrl)
        try container.encodeIfPresent(totalResults, forKey: .totalResults)
        try container.encodeIfPresent(fuelStations, forKey: .fuelStations)
   }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        stationLocatorUrl = try container.decodeIfPresent(String.self, forKey: .stationLocatorUrl)
        fuelStations = try container.decodeIfPresent(Array.self, forKey: .fuelStations)
        do {
            totalResults = try container.decodeIfPresent(String.self, forKey: .totalResults)
        } catch {
            totalResults = String(try container.decode(Int.self, forKey: .totalResults))
        }
    }

}


class FuelStation: Codable {
    
    var stationId: String?
    var accessCode: String?
    var accessDaysTime: String?
    var cardsAccepted: String?
    var fuelTypeCode: String?
    var ownerTypeCode: String?
    var statusCode: String?
    var stationName: String?
    var stationPhone: String?
    var facilityType: String?
    var latitude: Double?
    var longitude: Double?
    var city: String?
    var state: String?
    var streetAddress: String?
    var zip: String?
    var country: String?
    var evConnectorTypes: [String]?

    private enum CodingKeys: String, CodingKey {
        
        case stationId = "id"
        case accessCode = "access_code"
        case accessDaysTime = "access_days_time"
        case cardsAccepted = "cards_accepted"
        case fuelTypeCode = "fuel_type_code"
        case ownerTypeCode = "owner_type_code"
        case statusCode = "status_code"
        case stationName = "station_name"
        case stationPhone = "station_phone"
        case facilityType = "facility_type"
        case latitude
        case longitude
        case city
        case state
        case streetAddress = "street_address"
        case zip
        case country
        case evConnectorTypes = "ev_connector_types"

    }

    init() {
    }
    
    // MARK: - Codable
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try container.encodeIfPresent(stationId, forKey: .stationId)
        try container.encodeIfPresent(accessCode, forKey: .accessCode)
        try container.encodeIfPresent(accessDaysTime, forKey: .accessDaysTime)
        try container.encodeIfPresent(city, forKey: .city)
        try container.encodeIfPresent(state, forKey: .state)
        try container.encodeIfPresent(country, forKey: .country)
        try container.encodeIfPresent(cardsAccepted, forKey: .cardsAccepted)
        try container.encodeIfPresent(fuelTypeCode, forKey: .fuelTypeCode)
        try container.encodeIfPresent(ownerTypeCode, forKey: .ownerTypeCode)
        try container.encodeIfPresent(statusCode, forKey: .statusCode)
        try container.encodeIfPresent(stationName, forKey: .stationName)
        try container.encodeIfPresent(facilityType, forKey: .facilityType)
        try container.encodeIfPresent(latitude, forKey: .latitude)
        try container.encodeIfPresent(longitude, forKey: .longitude)
       try container.encodeIfPresent(streetAddress, forKey: .streetAddress)
       try container.encodeIfPresent(zip, forKey: .zip)
       try container.encodeIfPresent(evConnectorTypes, forKey: .evConnectorTypes)
   }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        do {
            stationId = try container.decodeIfPresent(String.self, forKey: .stationId)
        } catch {
            stationId = String(try container.decode(Int.self, forKey: .stationId))
        }
        accessCode = try container.decodeIfPresent(String.self, forKey: .accessCode)
        accessDaysTime = try container.decodeIfPresent(String.self, forKey: .accessDaysTime)
        cardsAccepted = try container.decodeIfPresent(String.self, forKey: .cardsAccepted)
        fuelTypeCode = try container.decodeIfPresent(String.self, forKey: .fuelTypeCode)
        stationName = try container.decodeIfPresent(String.self, forKey: .stationName)
        ownerTypeCode = try container.decodeIfPresent(String.self, forKey: .ownerTypeCode)
        city = try container.decodeIfPresent(String.self, forKey: .city)
        state = try container.decodeIfPresent(String.self, forKey: .state)
        country = try container.decodeIfPresent(String.self, forKey: .country)
        statusCode = try container.decodeIfPresent(String.self, forKey: .statusCode)
        stationName = try container.decodeIfPresent(String.self, forKey: .stationName)
        facilityType = try container.decodeIfPresent(String.self, forKey: .facilityType)
        latitude = try container.decodeIfPresent(Double.self, forKey: .latitude)
        longitude = try container.decodeIfPresent(Double.self, forKey: .longitude)
        streetAddress = try container.decodeIfPresent(String.self, forKey: .streetAddress)
        zip = try container.decodeIfPresent(String.self, forKey: .zip)
        evConnectorTypes = try container.decodeIfPresent(Array.self, forKey: .evConnectorTypes)

//        if let availIntegerValue = try container.decodeIfPresent(Int.self, forKey: .availableCount) {
//            availableCount = availIntegerValue
//        } else {
//            availableCount = 0
//        }
    }

}
