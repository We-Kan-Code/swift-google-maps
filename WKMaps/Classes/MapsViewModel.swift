//
//  MapsViewModel.swift
//  WKMaps
//
//  Created by Brian on 21/11/19.
//  Copyright © 2019 WeKan. All rights reserved.
//

import Foundation
import CoreLocation
import Alamofire

class MapsViewModel {
    
    var currentLocation: CLLocationCoordinate2D?
    var mapCenterLocation: CLLocationCoordinate2D?
    var visibleRadius: CLLocationDistance?
    var locationManager: CLLocationManager?
    var stations: Stations?
    
    func getNearestStations(onAPISuccess success: @escaping onApiSuccess, onAPIFailure failure: @escaping onApiFailure) {
        
        print("getNearestStations: \(String(describing: mapCenterLocation))")
        
        guard let centerLat = mapCenterLocation?.latitude else {
            return
        }
        guard let centerLongt = mapCenterLocation?.longitude else {
            return
        }
        let endPoint = Constants.EndPoint.nearestStations + "?api_key=\(AppConfig.Credentials.nrelPublicKey)&radius=\(visibleRadius ?? defaultRadiusInMiles)&latitude=\(centerLat)&longitude=\(centerLongt)"
        print(endPoint)
        NetworkHandler.getServiceCall(endPoint: endPoint, success: { (responseDict, statusCode) in
            print(responseDict)
            let jsonData = try! JSONSerialization.data(withJSONObject: responseDict, options: .prettyPrinted)
            let decodedData = try! JSONDecoder().decode(Stations.self, from: jsonData)
            self.stations = decodedData
            success("Success")
        }) { (errorMessage, errorCode) in
            print(errorMessage)
            failure(errorMessage)
        }
        
    }
    
    func cancelAnyOngoingRequests() {
        print("cancelAllOngoingRequests")
        
        Alamofire.SessionManager.default.session.getTasksWithCompletionHandler { (sessionDataTask, sessionUploadTask, sessionDownloadTask) in
            sessionDataTask.forEach {
                print("Cancel Task: \(sessionDataTask)")
                $0.cancel()
            }
        }
        
        Alamofire.SessionManager.default.session.getTasksWithCompletionHandler { (sessionDataTask, sessionUploadTask, sessionDownloadTask) in
            sessionDataTask.forEach {
                print(" Task State: \( $0.state) :  \(sessionDataTask)")
            }
        }
 
    }
    
}

