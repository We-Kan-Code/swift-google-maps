//
//  NetworkHandler.swift
//  WKBoilerPlate
//
//  Created by Brian on 21/08/19.
//  Copyright © 2019 Brian. All rights reserved.
//

import Foundation
import Alamofire

typealias onSuccess = (_ jsondata: Dictionary<String,Any>,_ statusCode: String) -> Void
typealias onFailure = (_ errorMessage: String,_ statusCode: String) -> Void
typealias onApiSuccess = (_ message: String) -> Void
typealias onApiFailure = (_ errorMessage: String) -> Void
typealias onTaskSuccess = (_ status: Bool) -> Void
typealias onTaskFailure = (_ errorMessage: String) -> Void
typealias onSessionExpiry = (_ errorMessage: String) -> Void

let TIME_OUT_INTERVAL = 30.0

class NetworkHandler {
    
    
    /**
     To get the authentication token for the user
     - Used in the headers of backend API calls
     - returns: authorization token as String
     */
    static func getBasicAuthentication() -> String? {
        guard let sessionToken: String = UserDefaults.standard.object(forKey: Constants.Defaults.authToken) as? String else {
            return ""
        }
        return (sessionToken.count == 0) ? "" : "Bearer \(sessionToken)"
    }
    
    /**
     Common method for all type of API calls, considering the parameters as a Dictionary
     - supported HTTP methods - GET, POST, PUT
     - Parameter endPoint: API endpoint
     - Parameter paramDict: all request parameters as a Dictionary
     - Parameter method: HTTP method type
     - Parameter onSuccess: success handler, return response JSON and status code
     - Parameter onFailure: error handler, return error message and error code
     */
    static func serviceCall(endPoint: String, paramDict: Dictionary<String,Any>, method: HTTPMethod, success onSuccess: @escaping onSuccess , failure onFailure: @escaping onFailure) -> Void {
        
        if NetworkReachabilityManager()!.isReachable == false {
            onFailure("Please check your internet connection","401")
        } else {
            var headers: HTTPHeaders = ["Content-Type":   "application/json"]
            if NetworkHandler.getBasicAuthentication()!.count > 0 {
                headers["Authorization"] =  NetworkHandler.getBasicAuthentication()!
            }
            
            let postUrl: String = AppConfig.getActiveBaseURL() + endPoint
            let request = NSMutableURLRequest(url: NSURL(string: postUrl)! as URL,
                                              cachePolicy: .useProtocolCachePolicy,
                                              timeoutInterval: TIME_OUT_INTERVAL)
            request.httpMethod = method.rawValue
            request.allHTTPHeaderFields = headers
            print(postUrl)
            print(paramDict)
            
            if paramDict.count > 0 {
                guard let httpBody = try? JSONSerialization.data(withJSONObject: paramDict, options: []) else {
                    return
                }
                request.httpBody = httpBody
            }
            
            let session = URLSession.shared
            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                
                let httpResponse = response as? HTTPURLResponse
                print(httpResponse as Any)
                
                if (error != nil) {
                    print(error as Any)
                    onFailure((error?.localizedDescription)!,"404")
                } else {
                    guard let statusCode = (httpResponse?.statusCode) else {
                        onFailure("Something went wrong, Please try again later","404")
                        return
                    }
                    
                    if data?.count == 0 {
                        // Handle the success status code with no-content
                        if statusCode == 204 {
                            onSuccess([:],String(statusCode));
                        } else {
                            onFailure("Unknown Error", String(statusCode))
                        }
                        return
                    }
                    
                    let responseData = try! JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                    if let dictFromJSON = responseData as? [String:Any] {
                        print(dictFromJSON as Any)

                        // For all success status codes, send the parsed data
                        if (statusCode > 199 && statusCode < 300 ) {
                            onSuccess(dictFromJSON,String(statusCode));
                        } else {
                            // error handling
                            NetworkHandler.handleErrorFromResponseJSON(jsonData: dictFromJSON, statusCode: statusCode, failureCallBack: { (errorMessage) in
                                onFailure(errorMessage, String(statusCode))
                            })
                            
                        }
                    }
                }
            })
            dataTask.resume()
        }
    }
    
    /**
     Common method for all type of API calls, considering the parameters in Data format
     - supported HTTP methods - GET, POST, PUT
     - Parameter endPoint: API endpoint
     - Parameter paramData: request parameters in Data format (possibly converted to data from model object)
     - Parameter method: HTTP method type
     - Parameter onSuccess: success handler, return response JSON and status code
     - Parameter onFailure: error handler, return error message and error code
     */
    static func serviceCall(endPoint: String, paramData: Data, method: HTTPMethod, success onSuccess: @escaping onSuccess , failure onFailure: @escaping onFailure) -> Void {
        
        if NetworkReachabilityManager()!.isReachable == false {
            onFailure("Please check your internet connection","401")
        } else {
            var headers: HTTPHeaders = ["Content-Type":   "application/json"]
            if NetworkHandler.getBasicAuthentication()!.count > 0 {
                headers["Authorization"] =  NetworkHandler.getBasicAuthentication()!
            }
            
            let postUrl: String = AppConfig.getActiveBaseURL() + endPoint
            let request = NSMutableURLRequest(url: NSURL(string: postUrl)! as URL,
                                              cachePolicy: .useProtocolCachePolicy,
                                              timeoutInterval: TIME_OUT_INTERVAL)
            request.httpMethod = method.rawValue
            request.allHTTPHeaderFields = headers
            print(postUrl)
            
            request.httpBody = paramData
            
            let session = URLSession.shared
            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                
                let httpResponse = response as? HTTPURLResponse
                print(httpResponse as Any)
                
                if (error != nil) {
                    print(error as Any)
                    onFailure((error?.localizedDescription)!,"404")
                } else {
                    
                    guard let statusCode = (httpResponse?.statusCode) else {
                        onFailure("Something went wrong, Please try again later","404")
                        return
                    }
                    
                    if data?.count == 0 {
                        // Handle the success status code with no-content
                        if statusCode == 204 {
                            onSuccess([:],String(statusCode));
                        } else {
                            onFailure("Unknown Error", String(statusCode))
                        }
                        return
                    }
                    
                    let responseData = try! JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                    if let dictFromJSON = responseData as? [String:Any] {
                        print(dictFromJSON as Any)

                        if (statusCode > 199 && statusCode < 300 ) {
                            onSuccess(dictFromJSON,String(statusCode));
                        } else {
                            // error handling
                            NetworkHandler.handleErrorFromResponseJSON(jsonData: dictFromJSON, statusCode: statusCode, failureCallBack: { (errorMessage) in
                                onFailure(errorMessage, String(statusCode))
                            })
                            
                        }
                    }
                }
            })
            dataTask.resume()
        }
    }
    
    /**
     Common method for GET calls
     - supported HTTP methods - GET
     - Parameter endPoint: API endpoint
     - Parameter onSuccess: success handler, return response JSON and status code
     - Parameter onFailure: error handler, return error message and error code
     */
    static func getServiceCall(endPoint:String, success onSuccess: @escaping onSuccess , failure onFailure: @escaping onFailure) ->Void{
        if NetworkReachabilityManager()!.isReachable == false {
            onFailure("Please Check your Internet Connection","401")
        }else{
            var headers: HTTPHeaders = [ "Accept":   "application/json",
                                         "Content-Type":   "application/json"]
            if NetworkHandler.getBasicAuthentication()!.count > 0 {
                headers["Authorization"] =   NetworkHandler.getBasicAuthentication()!
            }
            let postUrl: String = AppConfig.getActiveBaseURL() + endPoint
            print("postUrl:",postUrl)
            
            Alamofire.request(postUrl, method: .get, encoding: URLEncoding.default , headers: headers).responseJSON { response in
                
                guard let statusCode = response.response?.statusCode else {
                    return
                }
//                let statusCode = (response.response?.statusCode)!
                if response.result.isSuccess{
                    guard let jsonData: Dictionary<String,Any> = response.result.value as? Dictionary<String, Any> else {
                        return
                    }
//                    let jsonData : Dictionary<String,Any> = response.result.value! as! Dictionary
                    print(jsonData)
                    if(!jsonData.isEmpty){
                        if (statusCode > 199 && statusCode < 300 ){
                            onSuccess(jsonData,String(statusCode));
                        }else{
                            // error handling
                            NetworkHandler.handleErrorFromResponseJSON(jsonData: jsonData, statusCode: statusCode, failureCallBack: { (errorMessage) in
                                onFailure(errorMessage, String(statusCode))
                            })
                        }
                    }
                }else if response.result.isFailure{
                    onFailure(response.result.error!.localizedDescription,String(statusCode))
                    
                }
            }
        }
    }
    
    
    /**
     Method for posting Publications API with multipartFormData for image, video files
     - supported HTTP methods - POST
     - Parameter endPoint: API endpoint
     - Parameter paramDict: a dictionary with request parameters
     - Parameter filesDict: a dictionary with the files to upload
     - Parameter method: HTTP method type
     - Parameter onSuccess: success handler, return response JSON and status code
     - Parameter onFailure: error handler, return error message and error code
     */
    static func serviceCallForMultipartFormData(endPoint: String, paramDict: Dictionary<String,Any>, filesDict: Dictionary<String,Any>, fileType: Int, method: HTTPMethod, success onSuccess: @escaping onSuccess , failure onFailure: @escaping onFailure) -> Void {
        
        if NetworkReachabilityManager()!.isReachable == false {
            onFailure("Please check your internet connection","401")
        } else {
            var headers: HTTPHeaders = [ "Accept":   "application/json",
                                         "Content-Type":   "multipart/form-data"]
            if NetworkHandler.getBasicAuthentication()!.count > 0 {
                headers["Authorization"] =   NetworkHandler.getBasicAuthentication()!
            }
            
            let postUrl: String = AppConfig.getActiveBaseURL() + endPoint
            print(postUrl)
            print(paramDict)
            print(filesDict)
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                //params
                for(key,value) in paramDict {
                    multipartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key)
                }
                //files
                switch fileType {
                case Constants.UploadFileType.imageFile.rawValue:
                    for(key,value) in filesDict {
                        multipartFormData.append((value as! UIImage).pngData()!, withName: key, fileName: "content.png", mimeType: "image/png")
                    }
                    break
                case Constants.UploadFileType.videoFile.rawValue:
                    break
                    
                default:
                    break
                }
            }, usingThreshold: UInt64.init(), to: postUrl, method: method, headers: headers) { (encodingResult) in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        debugPrint(response)
                        let jsonData : Dictionary<String,Any> = response.result.value! as! Dictionary
                        let statusCode = (response.response?.statusCode)!
                        if(!jsonData.isEmpty){
                            // success response for statuscode 200 series
                            if (statusCode > 199 && statusCode < 300 ){
                                onSuccess(jsonData,String(statusCode));
                            }else{
                                // error handling for all other status codes
                                NetworkHandler.handleErrorFromResponseJSON(jsonData: jsonData, statusCode: statusCode, failureCallBack: { (errorMessage) in
                                    onFailure(errorMessage, String(statusCode))
                                })
                            }
                        }
                    }
                case .failure(let encodingError):
                    print(encodingError)
                    onFailure(encodingError.localizedDescription, "404")
                }
            }
        }
}

// MARK: - Response Error Handling

    /**
     Common method to handle all error Responses from APIs
     - Parameter jsonData: error dictionary
     - Parameter statusCode: HTTP error code or status code received
     - Parameter onFailure: error handler, returns error message to be displayed to user when required.
     */
    static func handleErrorFromResponseJSON(jsonData: Dictionary<String, Any>, statusCode: Int, failureCallBack onFailure: @escaping onTaskFailure) -> Void {
        
        //Handle Auth Token expiry
        if statusCode == 401 {
//            UserManager.shared.refreshToken { (success) in
                onFailure("Session restored. Please retry")
//            }
        } else {
            guard let msg = jsonData[Constants.ResponseKey.messages] as? String else {
                
                let errors = jsonData[Constants.ResponseKey.errors]
                if errors != nil {
                    if errors is Dictionary<String,Any> {
                        let errorDict = errors as! [String: Any]
                        let messages = errorDict[Constants.ResponseKey.messages] as! [String]
                        let errorMessage = messages[0]
                        onFailure(errorMessage)
                        return
                    }
                    else {
                        onFailure("Unknown Error")
                    }
                }
                else {
                    onFailure("Unknown Error")
                }
                return
            }
            onFailure(msg)
        }
        
    }
    
    
}
