//
//  Constants.swift
//  WKBoilerPlate
//
//  Created by Brian on 14/06/19.
//  Copyright © 2019 Brian. All rights reserved.
//

import Foundation
import UIKit

let defaultRadiusInMiles = 20.0
let defaultZoomLevel: Float = 14.0

class Constants {
    
    /** An instance for the AppDelegate which is UIApplication.shared.delegate */
    static let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
    
    /** List of all keys used in the app to store data in UserDefaults. */
    struct Defaults {
        static let authToken            = "auth_token"
  }
    
    /** List of all API Endpoints of webservices used in the app to communicate with the backend. */
    struct EndPoint {
        static let nearestStations       = "/api/alt-fuel-stations/v1/nearest.json"
    }
    
    /** List of all keys to be commonly handled from the response of our backend webservices. */
    struct ResponseKey {
        static let success              = "data"
        static let errors               = "errors"
        static let messages             = "messages"
    }
    
    /// List of all Storyboards used in the app.
    struct Storyboards {
        static let launchScreen         = "LaunchScreen"
        static let onboarding           = "Onboarding"
        static let main                 = "Main"
    }
    
    /// Enum with all types of Files the app supports for upload .
    /// - raw value : Int
    enum UploadFileType: Int {
        case text = 1
        case imageFile = 2
        case videoFile = 3
        case audioFile = 4
    }

}
