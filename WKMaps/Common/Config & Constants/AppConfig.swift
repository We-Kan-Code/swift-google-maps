//
//  AppConfig.swift
//  WKBoilerPlate
//
//  Created by Brian on 14/06/19.
//  Copyright © 2019 Brian. All rights reserved.
//

import Foundation
import UIKit

class AppConfig: NSObject {
    
    /** Gets the active Base URL to be used across app for backend API calls */
    static func getActiveBaseURL() -> String {
        return AppConfig.BaseUrl.DEV
    }
    
    /** List of Base URLs for development, QA, UAT, and Production environment. */
    struct BaseUrl {
        static let DEV              = "https://developer.nrel.gov"
        static let QA               = ""
        static let UAT              = ""
        static let PRODUCTION       = ""
    }
    
    
    /** Credentials of frameworks and features used in the app. */
    struct Credentials {
        static let googleMapsApiKey     = "AIzaSyA-fZIaxWfKRokSAlLai4SDzipdSAnuiRs"
        static let nrelPublicKey        = "DEMO_KEY"
    }

}
