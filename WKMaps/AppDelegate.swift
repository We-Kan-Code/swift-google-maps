//
//  AppDelegate.swift
//  WKMaps
//
//  Created by Brian on 21/11/19.
//  Copyright © 2019 WeKan. All rights reserved.
//

import UIKit
import GoogleMaps

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {


    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        GMSServices.provideAPIKey(AppConfig.Credentials.googleMapsApiKey)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let nav = storyboard.instantiateViewController(withIdentifier: "NavController") as! UINavigationController
        let vc = storyboard.instantiateViewController(withIdentifier: "MapsViewController") as! MapsViewController
        nav.viewControllers = [vc]
        window?.rootViewController = nav

        return true
    }


}

